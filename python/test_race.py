
import unittest
import race
import logging

logging.basicConfig(level=getattr(logging, 'INFO'))

data=eval("{u'race': {u'track': {u'pieces': [{u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'switch': True, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0}, {u'angle': -22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'radius': 200}, {u'angle': -22.5, u'radius': 200}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 62.0}, {u'angle': -45.0, u'switch': True, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 90.0}], u'lanes': [{u'index': 0, u'distanceFromCenter': -10}, {u'index': 1, u'distanceFromCenter': 10}], u'id': u'keimola', u'startingPoint': {u'position': {u'y': -44.0, u'x': -300.0}, u'angle': 90.0}, u'name': u'Keimola'}, u'cars': [{u'id': {u'color': u'red', u'name': u'Formula None'}, u'dimensions': {u'width': 20.0, u'length': 40.0, u'guideFlagPosition': 10.0}}], u'raceSession': {u'laps': 3, u'maxLapTimeMs': 60000, u'quickRace': True}}}")

class TestRace(unittest.TestCase):
  r = race.Race(data)

