
import track
import logging
import hashlib
import predict_throttle as pr
import formula_none_config as conf

class Race(object):

  def __init__(self, data):
    self.turbo_data=None
    self.race_stats=[]
    self.throttle=1.0
    self.hashkeys=dict()
    self.predict=pr.PredictThrottle()
    self.id = data['race']['track']['id']
    self.race_session = data['race']['raceSession']
    self.cars=data['race']['cars']
    self.dimensions=data['race']['cars'][0]['dimensions']
    self.name=data['race']['track']['name']
    self.track=track.Track(data['race']['track'])
    self.prev_distance=0.0
    self.prev_speed=0.0
    self.prev_pieces=[]         # Used during race to store ptrs to previous pieces
    self.prev_tick_piece=len(self.track.pieces)-1

    print("Start race on track %s" % (self.name))
    self.track.print_track()

  def get_speed(self, current_piece, current_distance, 
                current_lane, prev_piece):
    if current_distance >= self.prev_distance:
      current_speed = current_distance-self.prev_distance
    else:
      prev_piece = self.track.get_prev_index(current_piece)
      print('%f %f %f' % (current_distance, self.prev_distance, 
                          self.track.pieces[prev_piece].
                          get_piece_length(current_lane, self.track.switch_active)))
      current_speed = (current_distance + 
                       self.track.pieces[prev_piece].
                       get_piece_length(current_lane, self.track.switch_active) - 
                       self.prev_distance)
      # speed estimation has some bugs, therefore this check.
      if current_speed < 0:
        current_speed = current_distance
    print("Current speed is %f" % (current_speed))
    return current_speed
    

  def get_throttle(self, data):
    """
    Update throttle. Pretty awful function atm.
    """
    current_piece=data['piecePosition']['pieceIndex']
    current_lane=data['piecePosition']['lane']['startLaneIndex']
    target_lane=data['piecePosition']['lane']['endLaneIndex']
    current_lap=data['piecePosition']['lap']
    current_angle=data['angle']
    current_distance=data['piecePosition']['inPieceDistance']
    lane_switch='No'
    if current_lane != target_lane:
      self.track.switch_active=True
    else:
      self.track.switch_active=False
    current_speed=self.get_speed(current_piece, current_distance, 
                                 current_lane, self.prev_tick_piece)
    self.track.pieces[current_piece].update_angular_values(current_speed, current_lane)
    self.prev_distance = current_distance
    if current_piece != self.prev_tick_piece: # First tick in a new piece
      # Check if next piece is switch piece as switch has to be informed during
      # the previous piece.
      lane_switch=self.track.pieces[self.track.get_next_index(current_piece)].check_lane_switch(current_lane)

      prev_context=self.track.get_track_context(self.prev_tick_piece)
      prev_context_hash,prev_context_hash_data=(
        self.track.dataset.add_context_if_needed(
          self.track.pieces[self.prev_tick_piece], prev_context))
      self.track.pieces[self.prev_tick_piece].set_context_hash(
        prev_context_hash, prev_context_hash_data)

      context=self.track.get_track_context(current_piece)
      context_hash,context_hash_data=self.track.dataset.add_context_if_needed(
        self.track.pieces[current_piece], context)
      self.track.pieces[current_piece].set_context_hash(
        context_hash, context_hash_data)
      self.track.dataset.update_slip_angle(context_hash, current_angle, True)

      self.prev_pieces.append(self.track.pieces[self.prev_tick_piece])
      if self.track.dataset.get_max_slip(prev_context_hash): # Max slip detected
        print('Reducing entry speed for previous two pieces %s' % self.prev_pieces[-1])
        # self.track.dataset.reduce_entry_speed(prev_context_hash, 0.95)
        for i in xrange(-2,0):
          try:
            hashkey=self.prev_pieces[i].context_hash_key
            if hashkey != None:
              self.track.dataset.decrease_entry_speed(hashkey)
          except IndexError:
            pass
      else:
        print('Increasing entry speed for two previous pieces %s' % self.prev_pieces[-1])
        for i in xrange(-2,0):
          try:
            angle=abs(self.prev_pieces[i].angle)
            hashkey=self.prev_pieces[i].context_hash_key
            if hashkey != None:
              self.track.dataset.increase_entry_speed(hashkey, angle)
          except IndexError:
            pass
    else:
      context=self.track.get_track_context(current_piece)
      context_hash,context_hash_data=self.track.dataset.add_context_if_needed(
        self.track.pieces[current_piece], context)
      self.track.dataset.update_slip_angle(context_hash, current_angle, False)

    self.prev_tick_piece = current_piece
    distance, target_speed, next_bend_piece=(
      self.track.get_distance_to_next_bend(current_piece, 
                                           current_lane, current_distance))
    distance2, target_speed2, next_bend_piece2=(
      self.track.get_distance_to_next_bend(next_bend_piece, 
                                           current_lane, 0))
    distance3, target_speed3, next_bend_piece3=(
      self.track.get_distance_to_next_bend(next_bend_piece2, 
                                           current_lane, 0))
    turbo_on_or_off=False
    throttle=1.0
    # Check is throttle should be set to zero
    if (self.track.stop_throttle_or_not(current_speed, target_speed, distance) or
        self.track.stop_throttle_or_not(current_speed, target_speed2, distance+distance2) or
        self.track.stop_throttle_or_not(
          current_speed, target_speed3, distance+distance2+distance3) or
        abs(current_angle) > self.track.pieces[current_piece].context_hash_data['max_allowed_slip']):
      throttle = 0.0
    elif self.track.decide_turbo(self.turbo_data, current_piece, current_speed, target_speed):
      turbo_on_or_off = True
      self.turbo_data=None
    if turbo_on_or_off:
      print('New throttle is TURBO')
    else:
      print('New throttle is %f' % throttle)
  
    if (self.prev_speed != None and
        throttle == 0.0 and
        current_speed < self.prev_speed):
      try:
        ratio=current_speed/self.prev_speed
      except ZeroDivisionError:
        print('Error')
        ratio=conf.deceleration_factor
      print('Updating deceleration factor %f from %f to %f' %
            (ratio, conf.deceleration_factor, 0.98 * conf.deceleration_factor + 0.02 * ratio))
      conf.deceleration_factor = 0.98 * conf.deceleration_factor + 0.02 * ratio
    self.prev_speed = current_speed
    self.track.print_track_data()
    # Return new throttle value and lane_switch information
    return throttle, lane_switch, turbo_on_or_off

  def collect_stats(self):
    self.race_stats.append(self.track.get_stats())

  def print_stats(self):
    for lap in self.race_stats:
      print('%s' % lap)

  def finish_lap(self):
    self.collect_stats()

  def handle_crash(self):
    print('Crashed on piece %d' % 
          (self.prev_tick_piece))
    self.track.pieces[self.prev_tick_piece].print_piece()
    print('Reducing entry speed for previous two pieces %s' % self.prev_pieces[-1])
    # self.track.dataset.reduce_entry_speed(prev_context_hash, 0.95)
    for i in xrange(-2,0):
      try:
        hashkey=self.prev_pieces[i].context_hash_key
        if hashkey != None:
          self.track.dataset.decrease_entry_speed(hashkey)
          self.track.dataset.decrease_max_allowed_slip(hashkey)
          self.track.dataset.reduce_learning(hashkey)
      except IndexError:
        pass

  def handle_turbo(self, data):
    self.turbo_data=data

  def slow_learning(self):
    for key in self.track.dataset.dataset:
      self.track.dataset.dataset[key]['learning_rate'] *= 0.05
    return True
