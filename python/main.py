import json
import socket
import sys
import logging
import race
import pickle

import time

class Timer:    
    def __enter__(self):
        self.start = time.clock()
        return self

    def __exit__(self, *args):
        self.end = time.clock()
        self.interval = self.end - self.start

pickle_file="data/data.dat"

logging.basicConfig(level=getattr(logging, 'INFO'))
class FormulaNone(object):

    def __init__(self, socket, name, key, track='keimola'):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = track
        self.current_tick = 0
        self.my_car=None
        self.save_params=False

    def save_parameters(self):
        # return
        if self.save_params == False:
            return
        try:
            with open(pickle_file, 'wb') as of:
                pickle.dump(self.race.track.dataset, of, pickle.HIGHEST_PROTOCOL)
        except IOError:
            pass

    def load_parameters(self):
        try:
            with open(pickle_file, 'rb') as f:
                self.race.track.dataset = pickle.loads(f.read())
                self.race.track.update_hashkeys()
        except IOError:
            pass

    def msg(self, msg_type, data, tick=None):
        if tick == None:
            print('Sending msg: %s => %s' % 
                  (msg_type, data))
            self.send(json.dumps({"msgType": msg_type, "data": data}))
        else:
            print('Sending msg: %s => %s %d' % 
                  (msg_type, data, tick))
            self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": tick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        self.save_params = False
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
        
    def join_comp(self):
        self.save_params = False
        return self.msg("joinRace", {"botId": {"name": self.name,
                                               "key": self.key},
                                     "trackName": "germany",
                                     "password": "schumi4ever",
                                     "carCount": 3})
        
    def join_local(self):
        self.save_params = True
        return self.msg("joinRace", {"botId" : {"name": self.name,
                                                "key": self.key},
                                     "trackName" : self.track,
                                     "carCount" : 1,
                                     "laps" : 10,
                                     "password" : "foobar"})
        
    def throttle(self, throttle):
        self.msg("throttle", throttle, self.current_tick)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_comp(self):
        self.join_comp()
        self.msg_loop()

    def run_local(self):
        self.join_local()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
#        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_lap_finished(self, data):
        print("Lap finished %s" % data)
        self.race.finish_lap()
#        self.ping()

    def on_game_init(self, data):
        print("Game init %s\n%s" % (data,data['race']['raceSession']))
        if ('quickRace' not in data['race']['raceSession'] or
            data['race']['raceSession']['quickRace'] == True):
            self.race=race.Race(data)
            self.load_parameters()
        else:
            self.race.slow_learning()
#        self.ping()

    def on_car_positions(self, data):
        with Timer() as t:
            print("Car positions %s" % data)
            my_data=[x for x in data if x['id']['color'] == self.my_car][0]
            new_throttle=0.5
            new_throttle, lane_switch, turbo=self.race.get_throttle(my_data)
            if turbo:
                self.msg('turbo', 'Nnnnnooooooooooonnnnneeeeee')
            else:
                if lane_switch != 'No':
                    print('Lane switch %s' % lane_switch)
                    self.msg('switchLane', lane_switch)
                self.throttle(new_throttle)
        print('Request took %.03f sec.' % t.interval)            

    def on_crash(self, data):
        print("Someone crashed %s" % data)
        if data['color'] == self.my_car:
            self.race.handle_crash()
#        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.race.print_stats()
        self.save_parameters()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def your_car(self, data):
        self.my_car=data['color']
#        self.ping()

    def turbo_available(self, data):
        print('Turbo available %s' % data)
        self.race.handle_turbo(data)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished' : self.on_lap_finished,
            'yourCar' : self.your_car,
            'turboAvailable' : self.turbo_available,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            try:
                print("Game tick %d" % msg['gameTick'])
                self.current_tick = int(msg['gameTick'])
            except KeyError:
                pass
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Usage: ./run host port track botname botkey")
    else:
        host, port, name, track, key = sys.argv[1:6]
        print("Connecting with parameters:")
        print("host={0}, port={1}, track={3}, bot name={2}, key={4}".format(*sys.argv[1:6]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = FormulaNone(s, name, key, track)
        if track == 'CI':
            bot.run()
        elif track == 'comp':
            bot.run_comp()
        else:
            bot.run_local()
