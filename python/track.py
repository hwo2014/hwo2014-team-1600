
import math
import trackpiece
import logging
import sys
import formula_none_config as conf
import dataset

class Track(object):
  """ This is the track class that contains the track information
  """
  def __init__(self, data):
    self.switch_active=False
    self.tick_length=10.0       # Obsolete
    self.tick_count=10          # Obsolete
    self.pieces=[]
    self.count=len(data)
    self.dataset=dataset.DataSet()

    index=0
    # Add all pieces to track
    for d in data['pieces']:
      piece=trackpiece.TrackPiece(index,d, data['lanes'])
      index += 1
      self.pieces.append(piece)
    for p in self.pieces:
      context=self.get_track_context(p.index)
      context_hash,context_hash_data=(
        self.dataset.add_context_if_needed(
          self.pieces[p.index], context))
      self.pieces[p.index].set_context_hash(
        context_hash, context_hash_data)
      
    self.lanes=data['lanes']
    self.num_lanes=len(self.lanes)
    # Get lane change positions
    self.mark_lane_changes()

  def update_hashkeys(self):
    for p in self.pieces:
      context=self.get_track_context(p.index)
      context_hash,context_hash_data=(
        self.dataset.add_context_if_needed(
          self.pieces[p.index], context))
      self.pieces[p.index].set_context_hash(
        context_hash, context_hash_data)

  def get_distance_to_next_bend(self, index, lane, distance):
    """
    Calculate distance to next non-straight piece. This is needed
    in deciding when to turn off throttle.
    Return also the target entry speed
    """
    # Current piece is partly covered already
    length=self.pieces[index].get_piece_length(
      self.pieces[index].target_lane, self.switch_active)-distance
    while True:
      index += 1
      if index == len(self.pieces):
        index=0
      if self.pieces[index].length==None: # Curve, break
        break
      piece=self.pieces[index]
      length += piece.get_piece_length(piece.target_lane, self.switch_active)
    return length, self.pieces[index].context_hash_data['entry_speed'], index

  def stop_throttle_or_not(self, current_speed, target_speed, distance_to_bend):
    """
    Decide if throttle should be turned off
    """
    distance=0
    # Compute nr of ticks needed to change from current speed to target speed
    # with zero throttle
    try:
      ticks=math.log(target_speed/current_speed)/math.log(conf.deceleration_factor)
    except ZeroDivisionError:
      return False
    print('It takes %.1f ticks to decelerate to target speed' % ticks)
    if ticks < 0:               # We are under target speed
      return False
    speed=current_speed
    distance=speed
    for i in xrange(int(ticks)): # Get the distance traveled in ticks
      distance+=speed
      if distance >= distance_to_bend: # We reach curve with target speed
        print('Stopping throttle CS=%.1f TS=%.1f DTB=%.1f DIST=%.1f' %
              (current_speed, target_speed, distance_to_bend, distance))
        return True
      if speed < 1:             # We will be short of curve, full steam ahead
        print('Too slow: Keeping throttle CS=%.1f TS=%.1f DTB=%.1f DIST=%.1f' %
              (current_speed, target_speed, distance_to_bend, distance))
        return False
      speed *= conf.deceleration_factor
    print('Keeping throttle CS=%.1f TS=%.1f DTB=%.1f DIST=%.1f' %
          (current_speed, target_speed, distance_to_bend, distance))
    return False

  def get_next_piece(self, index):
    """
    Return next piece
    """
    index += 1
    if index >= len(self.pieces):
      index = 0
    return self.pieces[index]

  def print_track(self):
    for piece in self.pieces:
      piece.print_piece()

  def print_track_data(self):
    for piece in self.pieces:
      sys.stdout.write('(%d,%.1f,%d,%d,%.3f,%d)\n' % 
                       (piece.index, 
                        piece.context_hash_data['entry_speed'], 
                        int(piece.context_hash_data['max_slip']), 
                        int(piece.context_hash_data['max_allowed_slip']), 
                        piece.context_hash_data['learning_rate'], 
                        int(piece.angle)))
    sys.stdout.write('\n')

  def get_stats(self):
    stats=''
    for piece in self.pieces:
      stats += ('(%d,%.1f,%d,%d,%.3f,%d)\n' % 
                (piece.index, 
                 piece.context_hash_data['entry_speed'], 
                 int(piece.context_hash_data['max_slip']), 
                 int(piece.context_hash_data['max_allowed_slip']), 
                 piece.context_hash_data['learning_rate'], 
                 int(piece.angle)))
    return stats

  def get_next_index(self, index):
    """
    Get index of next piece
    """
    index += 1
    if index == len(self.pieces):
      return 0
    else:
      return index

  def get_prev_index(self, index):
    """
    Get index of previous piece
    """
    index -= 1
    if index == -1:
      return len(self.pieces)-1
    else:
      return index

  def mark_lane_changes(self):
    """
    Compute shortest path between two switch pieces and choose the lane
    accordingly.
    TODO: Refactor decision logic for tracks with more than 2 lanes
    """
    for i in xrange(0,len(self.pieces)):
      print('Piece ID=%d' % i)
      if self.pieces[i].switch==False:
        continue
      shortest_distance=1e6
      shortest_lane=0
      distances=[]
      for lane in xrange(self.num_lanes):
        distance=self.pieces[i].get_piece_length(lane, self.switch_active)/2.0
        piece=self.get_next_piece(i)
        while piece.switch == False:
          # print('%d Sub-dist=%f' % (lane,piece.get_piece_length(lane)))
          distance += piece.get_piece_length(lane, self.switch_active)
          piece=self.get_next_piece(piece.index)
        # print('Lane %d has length %f to next switch' %
        #       (lane, distance))
        distances.append(distance)
        if distance < shortest_distance:
          shortest_distance = distance
          shortest_lane=lane
      print('Shortest path to next switch is lane %d %f' %
            (shortest_lane, shortest_distance))
      if len(set(distances)) == 1:
        self.pieces[i].lane_switch='No'
      elif shortest_lane == 0:
        self.pieces[i].lane_switch='Left'
      else:
        self.pieces[i].lane_switch='Right'
      pp=i
      while True:
        if self.pieces[i].lane_switch=='No':
          self.pieces[pp].target_lane=self.pieces[self.get_prev_index(i)].target_lane
        else:
          self.pieces[pp].target_lane = shortest_lane
        pp=self.get_next_index(pp)
        if self.pieces[pp].switch:
          break

  def decide_turbo(self, data, current_piece, current_speed, target_speed):
    """
    Decide if turbo should be used on current tick
    Compute no. of ticks to next bend and use that as estimate.
    """
    if data==None:
      return False
    if self.pieces[current_piece].straight == False:
      return False
    min_ticks_to_bend=data['turboDurationTicks']
    try:
      ticks=math.log(target_speed/current_speed)/math.log(conf.deceleration_factor)
    except ZeroDivisionError:
      return False
    print('Turbo ticks %.1f <==> %.1f' %
          (min_ticks_to_bend, ticks))
    if self.check_all_straight(current_piece, conf.turbo_lookahead):
      print('Using turbo')
      return True
    return False

  def check_all_straight(self, current_piece, count):
    piece=self.pieces[current_piece]
    while piece.straight == True and count > 0:
      current_piece = self.get_next_index(current_piece)
      piece=self.pieces[current_piece]
      count -= 1
    if count == 0:
      return True
      
  def get_track_context(self, current_piece):
    context_length=3
    context=[]
    for i in xrange(-context_length,context_length+1):
      p = current_piece + i
      if p < 0:
        p += len(self.pieces)
      if p >= len(self.pieces):
        p -= len(self.pieces)
      context.append(self.pieces[p].get_curvature())
    print('Context for piece %d is %s' %
          (current_piece, context))
    return context
