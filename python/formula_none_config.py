# Variables for car
default_entry_speed_low=3.5     # Default entry speed for bend higher than 30 deg
default_entry_speed_high=6.5    # Default entry speed for bend lower than 30 deg
max_radius=200.0
min_radius=50.0
gradient=(default_entry_speed_high-default_entry_speed_low)/(max_radius-min_radius)

max_entry_speed=20.0            # Max entry speed, probably not needed
deceleration_factor=0.98        # Initial estimate of deceleration rate when throttle=0.0
max_slip=35.0                   # Maximum allowed slip before entry speed is lowered
max_slip_decrease_on_crash=0.8
max_slip_decrease=0.9
max_throttle=1.0
min_throttle=0.0

learning_rate=1.0
learning_rate_decay=0.97
decrease_multiplier=0.95

switch_length=2.2  # Estimate of switch length increase to current piece
turbo_lookahead=4

# These are not very important, but may be needed later
default_throttle=0.85
default_max_throttle=0.85

random_keep_lane_plan=0.70
random_dont_change_lane=0.15

acc_rate=0.9
dec_rate=0.3
max_throttle_update_threshold=20.0
default_value={'throttle' : default_throttle,
               'count' : 0,
               'max_throttle' : default_max_throttle,
               'mean_throttle' : None}

