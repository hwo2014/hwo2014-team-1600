
import formula_none_config as conf
import logging
import hashlib

class DataSet(object):

  def __init__(self):
    self.dataset=dict()

  def get_context_hash(self, context):
    h = hashlib.md5()
    for item in context:
      h.update(str(item))
    return h.hexdigest()

  def add_context_if_needed(self, current_piece, context):
    context_hash=self.get_context_hash(context)
    data=self.get_data(context_hash)
    if data == None:
      data = self.get_new_dataset(current_piece)
      self.dataset[context_hash]=data
    return context_hash, data

  def update_slip_angle(self, context_hash, angle, first_tick_in_piece):
    """
    Update slip angle stats for the current piece
    """
    data=self.get_data(context_hash)

    if first_tick_in_piece:
      data['max_slip'] = angle
    else:
      if angle > data['max_slip']:
        data['max_slip'] = angle

  def get_max_slip(self, context_hash):
    """
    Return true if slip has exceeded max_slip value. This could give a value between
    0 and 1 for angle between conf.max_slip and 60 deg.
    """
    try:
      return (abs(self.dataset[context_hash]['max_slip']) > 
              self.dataset[context_hash]['max_allowed_slip'])
    except KeyError:
      self.dataset[context_hash]['max_allowed_slip'] = conf.max_slip
      return (abs(self.dataset[context_hash]['max_slip']) > 
              self.dataset[context_hash]['max_allowed_slip'])

  def decrease_max_allowed_slip(self, context_hash):
    data=self.dataset[context_hash]
    data['max_allowed_slip'] *= conf.max_slip_decrease_on_crash

  def stop_learning(self, hashkey):
    data=self.dataset[hashkey]
    data['learning_rate'] = 0.0

  def reduce_learning(self, hashkey):
    data=self.dataset[hashkey]
    data['learning_rate'] *= 0.5

  def decrease_entry_speed(self, context_hash):
    """
    Entry speed update for this piece.
    """
    data=self.dataset[context_hash]
    multiplier=data['decrease_multiplier']
    print('Updating entry speed: T=%f M=%f' %
          (data['entry_speed'], multiplier))
    new_entry_speed = multiplier*data['entry_speed']
    if new_entry_speed < conf.default_entry_speed_low:
      new_entry_speed = conf.default_entry_speed_low
    data['entry_speed'] = new_entry_speed
    data['max_slip'] *= conf.max_slip_decrease

  def increase_entry_speed(self, context_hash, angle):
    """
    Entry speed update for this piece.
    """
    data=self.dataset[context_hash]
    learning_rate = data['learning_rate']
    multiplier=(1.0 + 
                data['learning_rate'] * 
                max(0.00, 0.10 - 0.08*((angle+45.0)/90.0)*((angle+45.0)/90.0)))
    print('Updating entry speed: T=%f M=%f' %
          (data['entry_speed'], multiplier))
    new_entry_speed = multiplier*data['entry_speed']
    if new_entry_speed > conf.max_entry_speed:
      new_entry_speed = conf.max_entry_speed
    data['entry_speed'] = new_entry_speed
    data['learning_rate'] *= conf.learning_rate_decay

  def get_data(self, context_hash):
    if context_hash in self.dataset:
      return self.dataset[context_hash]
    else:
      return None

  def get_new_dataset(self, piece):
    """
    Create new dataset based on current piece
    """
    d=dict()
    d['max_allowed_slip'] = conf.max_slip
    d['max_slip'] = 0.0
    d['decrease_multiplier'] = conf.decrease_multiplier
    d['learning_rate'] = conf.learning_rate
    if piece.straight:
      d['entry_speed'] = 20.0
    else:
      radius=abs(piece.radius)
      if radius < conf.min_radius:
        d['entry_speed'] = conf.default_entry_speed_low
      elif radius > conf.max_radius:
        d['entry_speed'] = conf.default_entry_speed_high
      else:
        d['entry_speed'] = conf.default_entry_speed_low + conf.gradient * (radius-conf.min_radius)
    return d


