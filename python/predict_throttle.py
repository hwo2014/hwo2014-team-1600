
import track
import logging
import formula_none_config as conf

class PredictThrottle(object):
  
  def __init__(self):
    self.futures=dict()
    self.angles=[]
    self.hashkey_sequence=[]
    self.current_tick=0
    self.prev_hash_key=None

  def get_new_throttle(self, hashdata, future_data, current_slip):
    hash_key=hashdata.hexdigest()
    self.angles.append(abs(current_slip))
    self.hashkey_sequence.append(hash_key)
    if hash_key not in self.futures:
      self.futures[hash_key] = dict(conf.default_value)
      self.futures[hash_key]['slip_data'] = list([0.0, 0.0, 0.0])
      self.futures[hash_key]['mean_throttle'] = conf.default_throttle
    current=self.futures[hash_key]
    if hash_key != self.prev_hash_key:
      current['throttle'] = current['mean_throttle']
    self.prev_hash_key=hash_key
    current['count'] += 1
    print('Updating throttle for data=%s H=%s CS=%f HK=%s DATA=%s' %
                 (future_data, hashdata, current_slip, hash_key, current))
    # current['slip_data'][0] = abs(current_slip)
    current['slip_data'][0] = 0.0
    max_slip=max(current['slip_data'])
    if max_slip > conf.max_throttle_update_threshold:
      current['max_throttle'] = 0.9 * current['max_throttle']
      if current['max_throttle'] < conf.min_throttle:
        current['max_throttle'] = conf.min_throttle
    else:
      current['max_throttle'] = 0.995 * current['max_throttle'] + 0.005 * conf.max_throttle

    if max_slip < conf.max_slip:
      new_throttle = (conf.acc_rate * current['throttle'] + (1.0-conf.acc_rate) * current['max_throttle'])
    else:
      new_throttle = conf.dec_rate * current['throttle'] + (1.0-conf.dec_rate) * conf.min_throttle
    current['throttle'] = new_throttle
    current['mean_throttle'] = 0.75 * current['mean_throttle'] + 0.25 * current['throttle']
    print('New throttle estimate=%f MT=%f with max_slip=%f' %
                 (current['throttle'], current['max_throttle'], max_slip))
    hash_key_min_five = self.current_tick-10
    hash_key_min_ten = self.current_tick-20
    self.current_tick += 1
    if hash_key_min_five >= 0:
      self.futures[self.hashkey_sequence[hash_key_min_five]]['slip_data'][1] = 0.25 * self.futures[self.hashkey_sequence[hash_key_min_five]]['slip_data'][1] + 0.75 * abs(current_slip)
    if hash_key_min_ten >= 0:
      self.futures[self.hashkey_sequence[hash_key_min_ten]]['slip_data'][2] = 0.25 * self.futures[self.hashkey_sequence[hash_key_min_ten]]['slip_data'][2] + 0.75 * abs(current_slip)
    # print('futures=%s angles=%s hash_keys=%s' %
    #       (self.futures, self.angles[-10:], self.hashkey_sequence[-10:]))
    # self.angles=self.angles[-100:]
    # self.hashkey_sequence=self.hashkey_sequence[-100:]
    return new_throttle
