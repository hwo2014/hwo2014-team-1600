# Class for track piece
"""
Contains information about the shape of piece
Also has control information for throttle, such as entry speed, throttle value

"""
import math
import formula_none_config as conf
import logging
import dataset

full_circle=360.0

class TrackPiece(object):
  """ This is the trackpiece class that contains the piece information
  """

  def __init__(self, index, data, lanes):
    self.index=index
    self.length=None
    self.lengths=[]
    self.switch=False
    self.straight=True
    self.radius=None
    self.angle=None
    self.lanes=lanes
    self.target_lane=0
    self.context_hash_key=None
    self.context_hash_data=None

    self.lane_switch="No"          # None=no switch, "Right"=l2r, "Left"=r2l

    if 'switch' in data:
      self.switch = True
    else:
      self.switch = False

    if 'length' in data:        # Straight piece
      self.length=data['length']
      self.angle=0.0
      self.radius=1.0e6
    elif 'radius' in data:      # Curve
      self.straight=False
      self.radius=data['radius']
      self.angle=data['angle']
    self.set_piece_lengths(lanes)

  def set_context_hash(self, context_hash, data):
    self.context_hash_key = context_hash
    self.context_hash_data = data

  def set_piece_lengths(self, lanes):
    """
    Precompute all piece lengths for each lane
    """
    for lane in lanes:
      if self.straight:         # Straight
        self.lengths.append(abs(self.length))
      else:                     # Curve
        radius=(self.radius - 
                lane['distanceFromCenter'] * math.copysign(1.0, self.angle))
        self.lengths.append(abs((float(self.angle)/full_circle)*float(radius)*2.0*math.pi))
    print('Lane lengths %s' % self.lengths)

  def print_piece(self):
    if self.straight:
      print('ID=%3d L=%4d S=%d SW=%s TL=%d' % 
            (self.index, self.length, self.switch, 
             self.lane_switch, self.target_lane))
    else:
      print('ID=%3d A=%f R=%f CL=%f C=%d S=%d TL=%d' % 
            (self.index, self.angle, self.radius, self.get_piece_length(0), 
             self.get_curvature(), self.switch, self.target_lane))

  def get_piece_length(self, lane, switch_active=False):
    """
    Get length of current piece, considering the lane
    """
    if switch_active:
      return self.lengths[lane]+conf.switch_length
    else:
      return self.lengths[lane]
    if self.straight:     # Straight
      return abs(self.length)
    else:
      radius=(self.radius - 
              self.lanes[lane]['distanceFromCenter'] * math.copysign(1.0, self.angle))
      return abs((float(self.angle)/full_circle)*float(radius)*2.0*math.pi)

  def get_curvature(self):
    """
    Value depicting the curve difficulty. Large angle and small radius make it
    more challenging, I suppose.
    """
    if self.straight:
      return 0
    else:
      radius=(self.radius + (self.lanes[self.target_lane]['distanceFromCenter'] * math.copysign(1.0, self.angle))) * math.copysign(1.0, self.angle)
      return int(radius)

  def check_lane_switch(self, lane):
    """
    Return 'No', 'Right' or 'Left' depending if lane change should be made
    """
    return self.lane_switch
    import random
    rand=random.random()
    if rand < conf.random_keep_lane_plan:
      return self.lane_switch 
    elif rand < conf.random_keep_lane_plan + conf.random_dont_change_lane:
      return 'No'
    else:
      if self.lane_switch=='Right':
        return 'Left'
      elif self.lane_switch=='Left':
        return 'Right'
      else:
        return 'No'

  def update_angular_values(self, current_speed, current_lane):
    radius=(self.radius - 
            self.lanes[current_lane]['distanceFromCenter'] * math.copysign(1.0, self.angle))
    if self.radius != None:
      self.angular_v = current_speed/radius
    else:
      self.angular_v = 0.0
    if self.radius != None:
      self.angular_a = (current_speed*current_speed)/radius
    else:
      self.angular_a = 0.0
    print('Angular values w=%.1f a=%.1f' % 
          (self.angular_v, self.angular_a))

